monithor_prowl.patch
====================
Adds Prowl push notifications to Monithor.
Apply with `patch -p1 < monithor_prowl.patch` while in the monithor/ directory.

**Please mind to add Prowl notifications to the database. You just need to do this once, of course.**

    INSERT INTO "settings" ("section", "field") VALUES ("notifications", "prowl1");
    INSERT INTO "settings" ("section", "field") VALUES ("notifications", "prowl2");



avrflash.sh
===========
Automates avr-gcc and avrdude

* Compiles and links object files
* Generates .hex
* Flashes .hex using avrdude
* Options are to be set in the script itself.

Requirements:

* avr-gcc
* avrdude



genmake.sh
==========
Generates dirty but working Makefiles

* Builds and links .o files from all source files
* Checks for .h inclusions
* Makefile supports complete build target and clean target
* Options are to be set in the script itself.

Requirements:

* POSIX compatible system
* bash
* gcc