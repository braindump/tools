#!/bin/bash

################################################################################
# Set build parameters here:


EXEC="avremu"
SRCDIR[0]="src"
SRCDIR[1]="src/asm"
BUILDDIR="build"
CC="gcc"
CFLAGS="-g -Wall"
MAKEFILE="Makefile"
FILEEXTENSION="c"		# cpp, c, cxx, whateveryouwant.



################################################################################
# Create new Makefile:

rm -rf Makefile
printf "CC = $CC\n" >> $MAKEFILE
printf "CFLAGS = $CFLAGS\n\n" >> $MAKEFILE



################################################################################
# Set up build for main executable:

printf "$EXEC:" >> $MAKEFILE
for i in ${SRCDIR[@]}
do
	for j in `ls $i/*.$FILEEXTENSION`
	do
		printf " $BUILDDIR/$j.o" >> $MAKEFILE
	done
done

printf "\n\t$CC $CFLAGS -o $EXEC" >> $MAKEFILE
for i in ${SRCDIR[@]}
do
	for j in `ls $i/*.$FILEEXTENSION`
	do
		printf " $BUILDDIR/$j.o" >> $MAKEFILE
	done
done
printf "\n\n" >> $MAKEFILE



########################################################################
# Set up build for dependencies:

for i in ${SRCDIR[@]}
do
	for j in `ls $i/*.$FILEEXTENSION`
	do
		gcc -MM -MG -MT $BUILDDIR/$j.o $j >> $MAKEFILE
		printf "\t$CC $CFLAGS -o $BUILDDIR/$j.o -c $j\n\n" >> $MAKEFILE
		if [ ! -d $BUILDDIR/`dirname $j` ]; then
			mkdir $BUILDDIR/`dirname $j`
		fi
	done
done



########################################################################
# Add clean target:

printf "clean:\n" >> $MAKEFILE
printf "\trm -rf $EXEC $BUILDDIR/*\n\n" >> $MAKEFILE
for i in ${SRCDIR[@]}
do
	printf "\tmkdir $BUILDDIR/$i\n" >> $MAKEFILE
done